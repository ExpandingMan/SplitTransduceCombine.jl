# SplitTransduceCombine

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/SplitTransduceCombine.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/SplitTransduceCombine.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/SplitTransduceCombine.jl/-/pipelines)

This package provides some of the functionality of
[SplitApplyCombine.jl](https://github.com/JuliaData/SplitApplyCombine.jl) using
[Transducers.jl](https://github.com/JuliaFolds2/Transducers.jl), primarily through simple
convenience functions.
