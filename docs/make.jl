using SplitTransduceCombine
using Documenter

DocMeta.setdocmeta!(SplitTransduceCombine, :DocTestSetup, :(using SplitTransduceCombine); recursive=true)

makedocs(;
    modules=[SplitTransduceCombine],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/SplitTransduceCombine.jl/blob/{commit}{path}#{line}",
    sitename="SplitTransduceCombine.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/SplitTransduceCombine.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
