
"""
    group(𝓀, 𝒻=identity)

A transducer that groups objects by `𝓀`.  The transformation `𝒻` is then applied to each.  This is an alias for
```julia
GroupBy(𝓀, Map(SingletonVector ∘ 𝒻 ∘ last)'(append!!))
```

## Examples
```julia
julia> 1:5 |> group(iseven) |> foldxl(right)
Transducers.GroupByViewDict{Bool,Vector{Int64},…}(...):
  0 => [1, 3, 5]
  1 => [2, 4]

julia> 1:5 |> group(iseven, x -> x+1) |> foldxl(right)
Transducers.GroupByViewDict{Bool,Vector{Int64},…}(...):
  0 => [2, 4, 6]
  1 => [3, 5]

julia> ss = ["capt kirk", "mr spock", "dr mccoy", "mr scott"];

julia> split.(ss) |> group(first, last) |> foldxl(right)
Transducers.GroupByViewDict{SubString{String},Vector{SubString{String}},…}(...):
  "mr"   => ["spock", "scott"]
  "dr"   => ["mccoy"]
  "capt" => ["kirk"]
```
"""
group(𝓀, 𝒻=identity) = GroupBy(𝓀, Map(SingletonVector ∘ 𝒻 ∘ last)'(append!!))


"""
    pgroupfind(𝓀)

A transducer that finds the index for objects in each group.  The upstream of this object are expected to be
pairs, and the keys (first components) of the pairs are what appear in the output arrays.

## Example
```julia
julia> pairs(["aa", "ab", "bc"]) |> pgroupfind(first) |> foldxl(right)
Transducers.GroupByViewDict{Char,Vector{Int64},…}(...):
  'a' => [1, 2]
  'b' => [3]
```
"""
function pgroupfind(𝓀)
    ed = Map(SingletonVector ∘ first ∘ last)'(append!!)
    GroupBy(𝓀 ∘ last, ed)
end


"""
    groupfind(𝓀; start=1, step=1)

A transducer that finds the enumeration (i.e. base-1) index for obejcts in each group.  The indices
returned are produced by `Enumerate()` and therefore only correspond to the object indices for sequentially
indexed axes starting at `1`.  For a transducer handling the general case see [`pgroupfind`](@ref).

## Example
```julia
julia> ["aa", "ab", "bc"] |> groupfind(first) |> foldxl(right)
Transducers.GroupByViewDict{Char,Vector{Int64},…}(...):
  'a' => [1, 2]
  'b' => [3]
```
"""
function groupfind(𝓀; start=1, step=1)
    xf1 = Enumerate(start, step)
    ed = Map(SingletonVector ∘ first ∘ last)'(append!!)
    xf2 = GroupBy(𝓀 ∘ last, ed)
    xf1 ⨟ xf2
end

#TODO: groupview, will probably need multiple forms of groupreduce
